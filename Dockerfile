FROM golang:alpine

RUN apk --no-cache add git openssl gcc libc-dev && \
    wget https://releases.hashicorp.com/terraform/0.12.5/terraform_0.12.5_linux_amd64.zip && \
    unzip terraform_0.12.5_linux_amd64.zip && \
    rm terraform_0.12.5_linux_amd64.zip && \
    mv terraform /usr/local/bin/terraform && \
    terraform -version && \
    go get github.com/gruntwork-io/terratest/modules/terraform && \
    go get -u github.com/golang/dep/cmd/dep && \
    cd $GOPATH/src/github.com/gruntwork-io/terratest/modules/terraform && go install

WORKDIR $GOPATH/src/app/test

ENTRYPOINT ["sh", "-c", "dep ensure -v && go test -v"]